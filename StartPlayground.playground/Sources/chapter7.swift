import Foundation

public func chapter7_3_1() {
    typealias MoveFunc = (Int) -> Int
    
    func goRight(_ currentPosition: Int) -> Int{
        return currentPosition+1
    }
    
    func goLeft(_ currentPosition: Int) -> Int{
        return currentPosition-1
    }
    
    func functionForMove(_ shouldLeft: Bool) -> MoveFunc{
        return shouldLeft ? goLeft:goRight
    }
    
    var position: Int = 3   //현위치
    
    let moveToZero: MoveFunc = functionForMove(position > 0)
    //현 위치가 0보다 크므로 전달되는 인자 값은 true가 됩니다.
    //그러므로 goLeft(_:)함수가 할당될 것입니다.
    
    print("원점으로 갑시다.")
    
    while position != 0 {
        print("\(position)...")
        position = moveToZero(position)
    }
    print("원점 도착!")
    
    
}

public func chapter7_3_2() {
    typealias MoveFunc = (Int) -> Int
    
    func functionForMove(_ shouldLeft: Bool) -> MoveFunc{
        
        func goRight(_ currentPosition: Int) -> Int{
            return currentPosition+1
        }
        
        func goLeft(_ currentPosition: Int) -> Int{
            return currentPosition-1
        }
        
        
        return shouldLeft ? goLeft:goRight
    }
    
    var position: Int = -4   //현위치
    
    let moveToZero: MoveFunc = functionForMove(position > 0)
    //현 위치가 0보다 크므로 전달되는 인자 값은 true가 됩니다.
    //그러므로 goLeft(_:)함수가 할당될 것입니다.
    
    print("원점으로 갑시다.")
    
    while position != 0 {
        print("\(position)...")
        position = moveToZero(position)
    }
    print("원점 도착!")
}

public func chapter7_4(){
    func crachAndBurn() -> Never{
        fatalError("SomeThing very, very bad happend!")
    }
    
    //crachAndBurn()
    
    func someFunction(isAllIsWell:Bool){
        guard isAllIsWell else{
            print("마을에 도둑이 들었습니다!")
            crachAndBurn()
        }
        print("All Is Well")
    }
    
    someFunction(isAllIsWell: true)
    someFunction(isAllIsWell: false)
    
}
