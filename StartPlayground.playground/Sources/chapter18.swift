import Foundation

public func chapter18_1(){
    class Person{
        var name: String = ""
        var age:Int = 0
        
        var introduction:String{
            return "이름 : \(name) 나이 : \(age)"
        }
        
        func speak(){
            print("가나다라마바사")
        }
    }
    
    let yagom: Person = Person()
    yagom.name = "yagom"
    yagom.age = 99
    print(yagom.introduction)
    yagom.speak()
}

public func chapter18_2(){
    class Person{
        var name: String = ""
        var age:Int = 0
        
        var introduction:String{
            return "이름 : \(name) 나이 : \(age)"
        }
        
        func speak(){
            print("가나다라마바사")
        }
    }
    
    class Student: Person{
        var grade: String = "F"
        
        func study(){
            print("Study hard...")
        }
    }
    
    let yagom: Person = Person()
    yagom.name = "yagom"
    yagom.age = 99
    print(yagom.introduction)
    yagom.speak()
    
    let jay: Student = Student()
    jay.name = "jay"
    jay.age = 10
    jay.grade = "A"
    print(jay.introduction)
    jay.speak()
    jay.study()
}

public func chapter18_4(){
    class Person{
        var name: String = ""
        var age:Int = 0
        
        var introduction:String{
            return "이름 : \(name) 나이 : \(age)"
        }
        
        func speak(){
            print("가나다라마바사")
        }
        
        class func introduceClass() -> String {
            return "인류의 소원은 평화입니다."
        }
    }
    
    class Student: Person{
        var grade: String = "F"
        
        func study(){
            print("Study hard...")
        }
        
        override func speak(){
            print("저는 학생입니다.")
        }
    }
    
    class UniversityStudent: Student {
        var major: String = ""
        
        class func introduceClass(){
            print(super.introduceClass())
        }
        
        override class func introduceClass() -> String{
            return "대학생의 소원은 A+입니다."
        }
        
        override func speak(){
            super.speak()
            print("대학생이죠.")
        }
    }
    
    let yagom: Person = Person()
    yagom.speak()
    
    let jay: Student = Student()
    jay.speak()
    
    let jenny: UniversityStudent = UniversityStudent()
    jenny.speak()
    
    print(Person.introduceClass())
    print(Student.introduceClass())
    print(UniversityStudent.introduceClass() as String)
    UniversityStudent.introduceClass() as Void
}

public func chapter18_5(){
    class Person{
        var name: String = ""
        var age:Int = 0
        var koreanAge: Int{
            return self.age+1
        }
        
        var introduction:String{
            return "이름 : \(name) 나이 : \(age)"
        }
    }
    
    class Student: Person{
        var grade: String = "F"
        
        override var introduction: String{
            return super.introduction + " " + "학점 : \(self.grade)"
        }
        
        override var koreanAge: Int{
            get{
                return super.koreanAge
            }
            
            set{
                self.age = newValue - 1
            }
        }
    }
    
    
    let yagom: Person = Person()
    yagom.name = "yagom"
    yagom.age = 55
    //yagom.koreanAge = 56
    print(yagom.introduction)
    print(yagom.koreanAge)
    
    let jay: Student = Student()
    jay.name = "jay"
    jay.age = 14
    jay.koreanAge = 15
    print(jay.introduction)
    print(jay.koreanAge)
}

/**
 * 프로퍼티 감시자 재정의
 */
public func chapter18_6(){
    class Person{
        var name: String = ""
        var age: Int = 0 {
            didSet{
                print("Person age : \(self.age)")
            }
        }
        
        var koreanAge: Int{
            return self.age + 1
        }
        
        var fullName: String{
            get{
                return self.name
            }
            
            set {
                self.name = newValue
            }
        }
    }
    
    class Student: Person {
        var grade: String = "F"
        
        override var age: Int {
            didSet{
                print("Student age : \(self.age)")
            }
        }
        
        override var koreanAge: Int{
            get{
                return super.koreanAge
            }
            
            set{
                self.age = newValue - 1
            }
        }
        
        override var fullName: String{
            didSet{
                print("Full Name : \(self.fullName)")
            }
        }
    }
    
    let yagom: Person = Person()
    yagom.name = "yagom"
    yagom.age = 55
    yagom.fullName = "Jo yagom"
    print(yagom.fullName)
    
    let jay: Student = Student()
    jay.name = "jay"
    jay.age = 14
    jay.koreanAge = 15
    jay.fullName = "Kim jay"
    print(jay.koreanAge)
}

/**
 * 서브스크립트의 재정의
 */
public func chapter18_7(){
    class Person{
        var name: String = ""
        var age: Int = 0 {
            didSet{
                print("Person age : \(self.age)")
            }
        }
        
        var koreanAge: Int{
            return self.age + 1
        }
        
        var fullName: String{
            get{
                return self.name
            }
            
            set {
                self.name = newValue
            }
        }
    }
    
    class Student: Person {
        var grade: String = "F"
        
        override var age: Int {
            didSet{
                print("Student age : \(self.age)")
            }
        }
        
        override var koreanAge: Int{
            get{
                return super.koreanAge
            }
            
            set{
                self.age = newValue - 1
            }
        }
        
        override var fullName: String{
            didSet{
                print("Full Name : \(self.fullName)")
            }
        }
    }
    
    class School {
        var students: [Student] = [Student]()
        
        subscript(number: Int) -> Student{
            print("School subscript")
            return students[number]
        }
    }
    
    class MiddleSchool: School {
        var middleStudents: [Student] = [Student]()
        
        override subscript(index: Int) -> Student{
            print("MiddleSchool subscript")
            return middleStudents[index]
        }
    }
    
    
    let university: School = School()
    university.students.append(Student())
    university[0]
    
    let middle: MiddleSchool = MiddleSchool()
    middle.middleStudents.append(Student())
    middle[0]
}

/**
 * final키워드의 사용
 */
public func chapter18_8(){
    class Person {
        final var name: String = ""
        
        final func speak(){
            print("가나다라마바사")
        }
    }
    /*
     final class Student: Person{
     //오류
     // Person의 name은.final을 사용하여 재정의 할 수 없도록 하였기 때문
     override var name: String{
     set{
     super.name = name
     }
     get{
     return "학생"
     }
     }
     
     override func speak(){
     print("저는 학생입니다.")
     }
     }
     
     class UniversityStudent: Student{}
     */
}

/**
 * 클래스 상속
 */
public func chapter18_9(){
    class Person{
        var name: String
        var age: Int
        
        init(name: String, age: Int) {
            self.name = name
            self.age  = age
        }
        
        convenience init(name: String){
            self.init(name: name, age: 0)
        }
    }
    
    class Student: Person{
        var major: String
        
        override init(name: String, age: Int){
            self.major = "Swift"
            super.init(name: name, age: age)
        }
        
        convenience init(name: String) {
            self.init(name: name, age: 7)
        }
    }
}

/**
 * 실패가능한 이니셜라이저의 재정의
 */
public func chapter18_11(){
    class Person{
        var name: String
        var age: Int
        
        init() {
            self.name = "Unknown"
            self.age  = 0
        }
        
        init?(name: String, age: Int){
            if name.isEmpty{
                return nil
            }
            
            self.name = name
            self.age = age
        }
        
        init?(age: Int){
            if age < 0 {
                return nil
            }
            
            self.name = "Unknown"
            self.age = age
        }
    }
    
    class Student: Person{
        var major: String
        
        override init?(name: String, age: Int){
            self.major = "Swift"
            super.init(name: name, age: age)
        }
        
        //부모는 실패가능 하더라도 자식은 실패 불가능하게 재정의 가능.
        override init(age: Int) {
            self.major = "Swift"
            super.init()
        }
    }
}


public func chapter18_12(){
    class Person{
        var name: String
        
        init(name: String) {
            self.name = name
        }
        
        convenience init(){
            self.init(name: "Unknown")
        }
    }
    
    class Student: Person {
        var major: String = "Swift"
    }
    
    // 부모클래스의 지정 이니셜라이저 자동 상속
    let yagom: Person = Person(name: "yagom")
    let hana: Student = Student(name: "hana")
    print(yagom.name)
    print(hana.name)
    
    // 부모클래스의 편의 이니셜라이져 자동 상속
    let wizplan: Person = Person()
    let jinSung: Student = Student()
    print(wizplan.name)
    print(jinSung.name)
}


/**
 * 편의 이니셜라이저의 자동 상속 -> 부모클래스의 지정 이니셜라이저를 모두 재정의 할 경우 부모의 모든 편의 이니셜라이저 사용 가능
 */
public func chapter18_13(){
    class Person {
        var name: String
        
        init(name: String){
            self.name = name
        }
        
        convenience init(){
            self.init(name: "Unknown")
        }
    }
    
    class Student: Person{
        var major: String
        
        override init(name: String) {
            self.major = "Unknown"
            super.init(name: name)
        }
        
        init(name: String, major: String) {
            self.major = major
            super.init(name: name)
        }
    }
    
    let wizplan: Person = Person()
    let jinSung: Student = Student()
    print(wizplan.name)
    print(jinSung.name)
}


/**
 * 편의 이니셜라이저 자동 상속 2
 */
public func chapter18_14(){
    class Person {
        var name: String
        
        init(name: String) {
            self.name = name
        }
        
        convenience init() {
            self.init(name: "Unknown")
        }
    }
    
    class Student: Person {
        var major: String
        
        convenience init(major: String) {
            self.init()
            self.major = major
        }
        
        override convenience init(name: String) {
            self.init(name: name, major: "Unknown")
        }
        
        init(name: String, major: String){
            self.major = major
            super.init(name: name)
        }
    }
    
    //부모클래스의 편의 이니셜라이저 자동 상속
    let wizplan: Person = Person()
    let jinSung: Student = Student()
    
    print(wizplan.name)
    print(jinSung.name)
    print(jinSung.major)
}

/**
 * 편의 이니셜라이저 자동 상속3
 */
public func chapter18_15(){
    class Person {
        var name: String
        
        init(name: String) {
            self.name = name
        }
        
        convenience init() {
            self.init(name: "Unknown")
        }
    }
    
    class Student: Person {
        var major: String
        
        convenience init(major: String) {
            self.init()
            self.major = major
        }
        
        override convenience init(name: String) {
            self.init(name: name, major: "Unknown")
        }
        
        init(name: String, major: String){
            self.major = major
            super.init(name: name)
        }
    }
    
    class UniversityStudent: Student {
        var grade: String = "A+"
        var description: String {
            return "\(self.name) \(self.major) \(self.grade)"
        }
        
        convenience init(name: String, major: String, grade: String) {
            self.init(name: name, major: major)
            self.grade = grade
        }
    }
    
    let nova: UniversityStudent = UniversityStudent()
    print(nova.description)
    
    let raon: UniversityStudent = UniversityStudent(name: "raon")
    print(raon.description)
    
    let joker: UniversityStudent = UniversityStudent(name: "joker", major: "Programming")
    print(joker.description)
    
    let chope: UniversityStudent = UniversityStudent(name: "chope", major: "Computer", grade: "C")
    print(chope.description)
}

// 요구이니셜라이저 정의
public func chapter18_16(){
    class Person {
        var name: String
        
        //요구 이니셜 라이저 정의
        required init(){
            self.name = "Unknown"
        }
    }
    
    class Student: Person {
        var major: String = "Unknown"
    }
    
    let miJeong: Student = Student()
}

// 요구 이니셜라이저 재구현
public func chapter18_17(){
    class Person {
        var name: String
        
        //요구 이니셜 라이저 정의
        required init(){
            self.name = "Unknown"
        }
    }
    
    class Student: Person {
        var major: String = "Unknown"
        
        init(major: String){
            self.major = major
            super.init()
        }
        
        required init() {
            self.major = "Unknown"
            super.init()
        }
    }
    
    class UniversityStudent: Student {
        var grade: String
        
        // 자신의 이니셜라이저 구현
        init(grade: String) {
            self.grade = grade
            super.init()
        }
        
        required init() {
            self.grade = "F"
            super.init()
        }
    }
    
    let jiSoo: Student = Student()
    print(jiSoo.major)
    
    let yagom: Student = Student(major: "Swift")
    print(yagom.major)
    
    let juHyun: UniversityStudent = UniversityStudent(grade: "A+")
    print(juHyun.grade)
}

public func chapter18_18(){
    class Person {
        var name: String
        
        init(){
            self.name = "Unknown"
        }
    }
    
    class Student: Person {
        var major: String = "Unknown"
        
        init(major: String){
            self.major = major
            super.init()
        }
        
        // 부모클래스의 이니셜라이저를 재정의함가 동시에 요구 이니셜라이저로 변경됨을 알립니다.
        required override init() {
            self.major = "Unknown"
            super.init()
        }
        
        // 이 요구 이니셜라이저는 앞으로 계속 요구됩니다.
        required convenience init(name: String) {
            self.init()
            self.name = name
        }
    }
    
    class UniversityStudent: Student {
        var grade: String
        
        // 자신의 이니셜라이저 구현
        init(grade: String) {
            self.grade = grade
            super.init()
        }
        
        required init() {
            self.grade = "F"
            super.init()
        }
        
        // Student 클래스에 서 요구하였으므로 구현해주어야 합니다.
        required convenience init(name: String){
            self.init()
            self.name = name
        }
    }
    
    let yagom: UniversityStudent = UniversityStudent()
    print(yagom.grade)
    
    let juHyun: UniversityStudent = UniversityStudent(name: "JuHyun")
    print(juHyun.name)

}









