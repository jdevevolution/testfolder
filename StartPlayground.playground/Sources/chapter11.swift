import Foundation

public func chapter11_1(){
    class SomeClass{
        init() {
            //초기화 시 필요한 코드
        }
    }
    
    struct SomeStruct{
        init() {
            // 초기화 시 필요한 코드
        }
    }
    
    enum SomeEnum{
        case someCase
        
        init() {
            // 열거형은 초기화 시 반드시 케이스 중 하나가 되어야 합니다.
            self = .someCase
            // 초기화 시 필요한 코드
        }
    }
}

public func chapter11_2(){
    struct Area{
        var squareMeter: Double
        
        init() {
            squareMeter = 0.0   // squareMeter의 초기값 할당
        }
    }
    
    let room: Area = Area()
    print(room.squareMeter) //0.0
}

public func chapter11_3(){
    struct Area{
        var squareMeter: Double = 0.0   //프로퍼티 초기값 할당
    }
    
    let room: Area = Area()
    print(room.squareMeter)
}

public func chapter11_4(){
    struct Area{
        var squareMeter: Double
        
        init(fromPy py: Double) {   //첫 번째 이니셜라이저
            squareMeter = py * 3.3058
        }
        
        init(fromSquareMeter squareMeter: Double) { //두 번째 이니셜라이저
            self.squareMeter = squareMeter
        }
        
        init(value: Double) {
            print("room Three")
            squareMeter = value
        }
        
        init(_ value: Double) { //네 번째 이니셜라이저
            print("room Four")
            squareMeter = value
        }
    }
    
    let roomOne: Area = Area(fromPy: 15.0)
    print(roomOne.squareMeter)
    
    let roomTwo: Area = Area(fromSquareMeter: 33.06)
    print(roomTwo.squareMeter)
    
    let roomThree: Area = Area(value: 30.0)
    let roomFour: Area = Area(55.0)
    
    //Area()
}

/*
 * 옵셔널 프로퍼티 타입
 */
public func chapter11_5(){
    class Person{
        var name: String
        var age: Int?
        
        init(name: String) {
            self.name = name
        }
    }
    
    let yagom: Person = Person(name: "yagom")
    
    print(yagom.name)
    print(yagom.age)
    
    yagom.age = 99
    
    print(yagom.age)
    
    yagom.name = "Eric"
    
    print(yagom.name)
}

/**
 * 초기화 위임
 */
public func chapter11_8(){
    enum Student {
        case elementary, middle, high
        case none
        
        // 사용자정의 이니셜라이저가 있는경우, init() 메서드를 구현해주어야 기본 이니셜라이저를 사용할 수 있습니다.
        init(){
            self = .none
        }
        
        init(koreanAge: Int) {
            switch koreanAge{
            case 8...13:
                self = .elementary
            case 14...16:
                self = .middle
            case 17...19:
                self = .high
            default:
                self = .none
            }
        }
        
        init(bornAt: Int, currentYear: Int) {
            self.init(koreanAge: currentYear-bornAt+1)
        }
    }
    
    var younger: Student = Student(koreanAge: 16)
    print(younger)
    
    younger = Student(bornAt: 1998, currentYear: 2016)
    print(younger)
}

/**
 * 실패 가능한 이니셜라이저
 */
public func chapter11_9(){
    class Person{
        let name: String
        var age: Int?
        
        init?(name: String) {
            if name.isEmpty{
                return nil
            }
            self.name = name
        }
        
        init?(name: String, age: Int){
            if name.isEmpty || age < 0 {
                return nil
            }
            self.name = name
            self.age = age
        }
    }
    
    let yagom: Person? = Person(name: "yagom", age: 99)
    
    if let person: Person = yagom{
        print(person.name)
    }else{
        print("Person wasn't initialized")
    }
    
    let chop: Person? = Person(name: "chop", age: -10)
    
    
    if let person: Person = chop{
        print(person.name)
    }else{
        print("Person wasn't initialized")
    }
    
    let eric: Person? = Person(name: "", age: -10)
    
    
    if let person: Person = eric{
        print(person.name)
    }else{
        print("Person wasn't initialized")
    }
}

/**
 * 열거형 실패 가능한 이니셜라이저
 */
public func chapter11_10(){
    enum Student: String {
        case elementary = "초등학생", middle = "중학생", high = "고등학생"
        
        init?(koreanAge: Int) {
            switch koreanAge{
            case 8...13:
                self = .elementary
            case 14...16:
                self = .middle
            case 17...19:
                self = .high
            default:
                return nil
            }
        }
        
        init?(bornAt: Int, currentYear: Int){
            self.init(koreanAge: currentYear - bornAt + 1)
        }
    }
    
    var younger: Student? = Student(koreanAge: 20)
    print(younger)
    
    younger = Student(bornAt: 2020, currentYear: 2016)
    print(younger)
    
    younger = Student(rawValue: "대학생")
    print(younger)
    
    younger = Student(rawValue: "초등학생")
    print(younger)
    
}

public func chapter11_12(){
    struct Student{
        var name: String?
        var number: Int?
    }
    
    class SchoolClass{
        var students: [Student] = {
            // 새로운 인스턴스를 생성하고 사용자정의 연산을 통한 후 반환해줍니다.
            // 반환되는 값의 타입은 [Student] 타입이어야 합니다.
            
            var arr: [Student] = [Student]()
            
            for num in 1...15{
                var student: Student = Student(name: nil, number: nil)
                arr.append(student)
            }
            return arr
        }()
    }
    
    let myClass: SchoolClass = SchoolClass()
    print(myClass.students.count)
}

public func chapter11_14(){
    class FileManager{
        var fileName: String
        
        init(fileName: String) {
            self.fileName = fileName
        }
        
        func openFile(){
            print("Open File : \(self.fileName)")
        }
        
        func modifyFile(){
            print("Modify File : \(self.fileName)")
        }
        
        func writeFile(){
            print("Write File : \(self.fileName)")
        }
        
        func closeFile(){
            print("Close File : \(self.fileName)")
        }
        
        deinit {
            print("Deinit instance")
            self.writeFile()
            self.closeFile()
        }
    }
    
    var fileManager: FileManager? = FileManager(fileName: "abc.txt")
    
    if let manager: FileManager = fileManager {
        manager.openFile()
        manager.modifyFile()
    }
    
    fileManager = nil
}
