import Foundation

func hello(name: String) -> String{
    return "Hello \(name)"
}

let helloJeny: String = hello(name: "Jenny")
//print(helloJeny)
