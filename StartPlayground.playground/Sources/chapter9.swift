import Foundation

struct BasicInfomation {
    var name: String
    var age : Int
}

public func goChapter9_1(){
    var yagomInfo: BasicInfomation = BasicInfomation(name: "yagom", age: 99)
    
    yagomInfo.age = 30
    yagomInfo.name = "Seba"
    
    print(yagomInfo)
    
    var jennyInfo: BasicInfomation = BasicInfomation(name: "Seba", age: 99)
    
    jennyInfo.age = 22  //let으로 선언할 경우 변경 불가
    
    
}

class Person {
    var height : Float = 0.0
    var weight : Float = 0.0
    
    deinit {
        print("Person 클래스의 인스턴스가 소멸됩니다.")
    }
}

public func goChapter9_2(){
    var yagom: Person? = Person()
    yagom?.height = 19
    //yagom = nil
    print(yagom)
}
