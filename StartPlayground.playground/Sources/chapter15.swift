import Foundation

/**
 * 15. 맵, 필터, 리듀스
 **/

//15.1 맵
/**
 * for - in 구문과 맵 메서트의 사용 비교
 */
public func chapter15_1(){
    let numbers: [Int] = [0, 1, 2, 3, 4]
    
    var doubleNumbers: [Int] = [Int]()
    var strings: [String] = [String]()
    
    //for 구문 사용
    for number in numbers {
        doubleNumbers.append(number * 2)
        strings.append("\(number)")
    }
    
    print(doubleNumbers)
    print(strings)
    
    //map 사용
    doubleNumbers = numbers.map({ (number: Int) -> Int in
        return number * 2
    })
    strings = numbers.map({ (number: Int) -> String in
        return "\(number)"
    })
    
    print(doubleNumbers)
    print(strings)
}

/**
 * 클로저 푶현의 간략화
 */
public func chapter15_2(){
    let numbers: [Int] = [0,1,2,3,4]
    
    // 기본 클로저 표현식 사용
    var doubleNumbers = numbers.map({ (number: Int) -> Int in
        return number * 2
    })
    
    // 매개변수 및 변환 타입 생략
    doubleNumbers = numbers.map({ return $0 * 2 })
    print(doubleNumbers)
    
    // 반환키워드 생략
    doubleNumbers = numbers.map({ $0 * 2 })
    print(doubleNumbers)
    
    // 후행클로저 사용
    doubleNumbers = numbers.map{ $0 * 2 }
    print(doubleNumbers)
}

/*
 * 클로저의 반복 사용
 */
public func chapter15_3(){
    let evenNumbers: [Int] = [0,2,4,6,8]
    let oddNumbers: [Int] = [0,1,3,5,7]
    let multiplyTwo: (Int) -> Int = {$0 * 2}
    
    let doubleEvenNumbers = evenNumbers.map(multiplyTwo)
    print(doubleEvenNumbers)
    
    let doubleOddNumbers = oddNumbers.map(multiplyTwo)
    print(doubleOddNumbers)
}

/**
 * 다양한 컨테이너 타입에서의 맵의 활용
 */
public func chapter15_4(){
    let alphabetDictionary: [String: String] = ["a": "A", "b": "B"]
    
    var keys: [String] = alphabetDictionary.map{(tuple: (String, String)) -> String in
        return tuple.0
    }
    
    keys = alphabetDictionary.map{ $0.0 }
    
    let values: [String] = alphabetDictionary.map{ $0.1 }
    print(keys)
    print(values)
    
    var numberSet: Set<Int> = [1, 2, 3, 4, 5]
    let resultSet = numberSet.map{$0*2}
    print(resultSet)
    
    let optionalInt: Int? = 3
    let resultInt: Int? = optionalInt.map{$0*2}
    print(resultInt)
    
    let range: CountableClosedRange = (0...3)
    let resultRange: [Int] = range.map{$0*2}
    print(resultRange)
}

/**
 * 필터 메서드의 사용
 */
public func chapter15_5(){
    let numbers: [Int] = [0,1,2,3,4,5]
    
    let evenNumbers: [Int] = numbers.filter{(number: Int) -> Bool in
        return number%2 == 0
    }
    print(evenNumbers)
    
    let oddNumbers: [Int] = numbers.filter{ $0%2 != 0 }
    print(oddNumbers)
}

/**
 * 맵과 필터의 메서드의 연계사용
 */
public func chapter15_6(){
    let numbers: [Int] = [0, 1, 2, 3, 4, 5]
    
    let mappedNumbers: [Int] = numbers.map{ $0 + 3 }
    
    let evenNumbers: [Int] = mappedNumbers.filter{ (number: Int) -> Bool in
        return number % 2 == 0
    }
    print(evenNumbers)
    
    //mappedNumbers가 굳이 여러 번 사용될 필요가 없다면 메서드를 체인처럼 연결하여 사용할 수 있습니다.
    let oddNumbers: [Int] = numbers.map{ $0+3 }.filter{ $0%2 != 0 }
    print(oddNumbers)
}

/**
 * 리듀스 메서드의 사용
 */
public func chapter15_7(){
    let numbers: [Int] = [1, 2, 3]
    
    //초깃값이 0이고 정수 배열의 모든 값을 더함
    var sum: Int = numbers.reduce(0, {(first: Int, second: Int) -> Int in
        print("\(first) + \(second)")
        return first+second
    })
    
    print(sum)
    
    // 초기값이 0이고 정수 배열의 모든 값을 뺍니다.
    var subtract: Int = numbers.reduce(0, {(first: Int, second: Int) -> Int in
        print("\(first) - \(second)")
        return first-second
    })
    
    print(subtract)
    
    // 초기값이 3이고 정수 배열의 모든값을 더합니다.
    let sumFromThree: Int = numbers.reduce(3){
        print("\($0) + \($1)")
        
        return $0 + $1
    }
    print(sumFromThree)
    
    let subtractFromThree: Int = numbers.reduce(3){
        print("\($0) - \($1)")
        return $0-$1
    }
    print(subtractFromThree)
    
    // 문자열 배열을 reduce(_: _:) 메서드를 통해 연결시킵니다.
    let names: [String] = ["Chope", "Jay", "Joker", "Nova"]
    
    let reducedNames: String = names.reduce("yagom's friends : "){
        return $0 + ", "+$1
    }
    print(reducedNames)
}

/**
 * 맵, 필터, 리듀스 메서드의 연계사용
 */
public func chapter15_8(){
    let numbers: [Int] = [1, 2, 3, 4, 5, 6, 7]
    
    // 짝수를 걸러내어 각 값에 3을 곱해준 후 모든 값을 더합니다.
    var result: Int = numbers.filter{ $0 % 2 == 0 }.map{ $0 * 3 }.reduce(0){ $0 + $1 }
    print(result)
    
}

/**
 * 친구들의 정보 생성
 */
public func chapter15_9(){
    enum Gender{
        case male, female, unknown
    }
    
    struct Friend {
        let name: String
        let gender: Gender
        let location: String
        var age: UInt
    }
    
    var friends: [Friend] = [Friend]()
    
    friends.append(Friend(name: "Yoobato", gender: .male, location: "발리", age: 26))
    friends.append(Friend(name: "JiSoo", gender: .male, location: "시드니", age: 24))
    friends.append(Friend(name: "JuHyun", gender: .male, location: "경기", age: 30))
    friends.append(Friend(name: "JiYoung", gender: .female, location: "서울", age: 22))
    friends.append(Friend(name: "SungHo", gender: .male, location: "충북", age: 20))
    friends.append(Friend(name: "JungKi", gender: .unknown, location: "대전", age: 29))
    friends.append(Friend(name: "YoungMin", gender: .male, location: "경기", age: 24))
    
    var result: [Friend] = friends.map{ Friend(name: $0.name, gender: $0.gender, location: $0.location, age: $0.age+1)}
    result = result.filter{ $0.age >= 25 && $0.location != "서울"}
    let resultStr: String = result.reduce("서울 외의 지역에 거주하면서 25세 이상인 친구"){
        $0 + "\n \($1.name) \($1.gender) \($1.location) \($1.age)세"
    }
    
    print(resultStr)
}
