import Foundation

func chapter10_1(){
    struct CoordinatePoint{
        var x : Int
        var y : Int
    }
    
    //구조체는 기본적으로 저장 프로퍼티를 매개변수로 가지는 이니셜라이저가 있습니다.
    let yagomPoint: CoordinatePoint = CoordinatePoint(x: 10, y: 5)
    
    //사람의 위치정보
    class Position{
        var point: CoordinatePoint
        
        let name: String
        
        
        init(name: String, currentPoint: CoordinatePoint) {
            self.name = name
            self.point = currentPoint
        }
    }
    
    // 사용자 정의 이니셜라이저를 호출해야만한다.
    // 그렇지 않으면 프로퍼티 초깃값을 할당할 수 없기 때문에 인스턴스 생성이 불가능하다.
    let yagomPosition: Position = Position(name: "yagom", currentPoint: yagomPoint)
}

func chapter10_2(){
    struct CoordinatePoint {
        var x: Int = 0  // 저장프로퍼티
        var y: Int = 0  // 저장프로퍼티
    }
    
    // 프로퍼티의 초깃값을 할당했다면 굳이 전달인자로 초깃값을 넘길 필요가 없습니다.
    let yagomPoint: CoordinatePoint = CoordinatePoint()
    // 물론 기존에 초깃값을 할당할 수 있는 이니셜라이저도 사용 가능
    let wizplanPoint: CoordinatePoint = CoordinatePoint(x: 10, y:5)
    
    print("yagome's point : \(yagomPoint.x), \(yagomPoint.y)")
    
    print("wizplan's point : \(wizplanPoint.x), \(wizplanPoint.y)")
    
    class Position{
        var point: CoordinatePoint = CoordinatePoint()
        var name: String = "Unknown"
    }
    
    // 초기값을 지정해줬다면 사용자정의 이니셜라이저를 사용하지 않아도 됩니다.
    let yagomPosition: Position = Position()
    
    yagomPosition.point = yagomPoint
    yagomPosition.name = "yagom"
    
}

func chapter10_3(){
    struct CoodinatePoint{
        var x:Int
        var y:Int
    }
    
    class Position{
        var point: CoodinatePoint?  //현재 사람의 위치를 모를 수도 있습니다 - 옵셔널
        let name:String
        
        init(name: String) {
            self.name = name
        }
    }
    // 이름은 필수지만 위치는 모를 수 있습니다.
    let yagomPosition: Position = Position(name: "yagom")
    
    // 위치를 알게되면 그 때 위치 값을 할당해 줍니다.
    yagomPosition.point = CoodinatePoint(x:20, y:10)
    
}

func chapter10_4(){
    struct CoordinatePoint {
        var x: Int = 0
        var y: Int = 0
    }
    
    class Position{
        lazy var point: CoordinatePoint = CoordinatePoint()
        let name: String
        
        init(name: String){
            self.name = name
        }
    }
    
    let yagomPosition: Position = Position(name: "yagom")
    
    print(yagomPosition.point)
}


/**
 * 연산프로퍼티
 */
public func chapter10_5(){
    struct CoordinatePoint{
        var x: Int
        var y: Int
        
        //대칭점을 구하는 메서트 - 접근자
        func oppositePoint() -> CoordinatePoint {
            return CoordinatePoint(x: -x, y: -y)
        }
        
        //대칭점을 설정하는 메서드 - 설정자
        // mutating 키워드에 관한 애용은 메스트(198쪽)에서 다룹니다.
        mutating func setOppositePoint(_ opposite: CoordinatePoint){
            x = -opposite.x
            y = -opposite.y
        }
    }
    
    var yagomPosition: CoordinatePoint = CoordinatePoint(x: 10, y:20)
    
    //현재좌표
    print(yagomPosition)
    
    //대칭좌표
    print(yagomPosition.oppositePoint())
    
    //대칭 좌표를 (15,10)으로 설정하면
    yagomPosition.setOppositePoint(CoordinatePoint(x:15, y:10))
    
    print(yagomPosition)
}

public func chapter10_6(){
    struct Coordinate{
        var x: Int
        var y: Int
        
        //대칭좌표
        var oppositePoint: Coordinate{
            //접근자
            get{
                return Coordinate(x: -x, y: -y)
            }
            //설정자
            set(opposite){
                x = -opposite.x
                y = -opposite.y
            }
        }
    }
    
    var yagomPosition: Coordinate = Coordinate(x: 10, y: 20)
    
    //현재 좌표
    print(yagomPosition)
    
    //대칭 좌표
    print(yagomPosition.oppositePoint)
    
    //대칭 좌표를(15, 20)으로 설정하면
    yagomPosition.oppositePoint = Coordinate(x: 15, y: 20)
    
    //현재 좌표는 -15, -20으로 설정됩니다
    print(yagomPosition)
}

public func chapter10_7(){
    struct CoordinatePoint{
        var x: Int
        var y: Int
        
        // 대칭 좌표
        var oppositePoint: CoordinatePoint {
            get{
                return CoordinatePoint(x: -x, y: -y)
            }
            set{
                x = -newValue.x
                y = -newValue.y
            }
        }
    }
}

public func chapter10_8(){
    struct CoordinatePoint{
        var x: Int
        var y: Int
        
        // 대칭 좌표
        var oppositePoint: CoordinatePoint {
            get{
                return CoordinatePoint(x: -x, y: -y)
            }
        }
    }
    
    var yagomPosition : CoordinatePoint = CoordinatePoint(x: 10, y: 20)
    
    print(yagomPosition)
    
    print(yagomPosition.oppositePoint)
    
    //설정자를 구현하지 않았으므로 오류!!
    //yagomPosition.oppositePoint = CoordinatePoint(x: 15, y:10)
}

public func chapter10_9(){
    class Account{
        var credit: Int = 0 {
            //willSet에서 전달 인자는 변경될 값
            willSet{
                print("잔액이 \(credit)원에서 \(newValue)원으로 변경될 예정입니다.")
            }
            //didSet에서 전달 인자는 변경되기 전의 값
            didSet{
                print("잔액이 \(oldValue)원에서 \(credit)원으로 변경되었습니다.")
            }
        }
    }
    
    let myAccount: Account = Account()
    
    //myAccount.credit = 1000
}

public func chapter10_10(){
    class Account{
        var credit:Int = 0{
            //willSet에서 전달 인자는 변경될 값
            willSet{
                print("잔액이 \(credit)원에서 \(newValue)원으로 변경될 예정입니다.")
            }
            //didSet에서 전달 인자는 변경되기 전의 값
            didSet{
                print("잔액이 \(oldValue)원에서 \(credit)원으로 변경되었습니다.")
            }
        }
        
        var dollarValue: Double{
            get{
                return Double(credit)/1000.0
            }
            
            set{
                credit = Int(newValue*1000)
                print("잔액을 \(newValue)달러로 변경 중입니다.")
            }
        }
    }
    
    class ForeignAccount: Account{
        override var dollarValue: Double{
            willSet{
                print("잔액이 \(dollarValue)달러에서 \(newValue)달러로 변경될 예정입니다.")
            }
            
            didSet{
                print("잔액이 \(oldValue)달러에서 \(dollarValue)달러로 변경되었습니다.");
            }
        }
    }
    
    let myAccount : ForeignAccount = ForeignAccount()
    myAccount.credit = 1000
    // 잔액이 0원에서 1000원으로 변경되었습니다.
    
    //잔액이 1.0달러에서 2.0달러로 변경될 예정입니다.
    //잔액이 1000원에서 2000원으로 변경될 예정입니다.
    //잔액이 1000원에서 2000원으로 변경되었습니다.
    
    myAccount.dollarValue = 2 //잔액을 2.0달러롤 변경 중입니다.
    //잔액이 1.0달러에서 2.0달러로 변경되었습니다.
}


public func chapter10_11(){
    var wonInPoket: Int = 2000 {
        willSet{
            print("주머니의 돈이\(wonInPoket)원에서 \(newValue)원으로 변경될 예정입니다.")
        }
        
        didSet{
            print("주머니의 돈이 \(oldValue)원에서 \(wonInPoket)원으로 변경되었습니다.")
        }
    }
    
    var dollarInPoket: Double{
        get{
            return Double(wonInPoket)
        }
        set{
            wonInPoket = Int(newValue*1000.0)
            print("주머니의 달러를 \(newValue)달러로 변경 중입니다.")
        }
    }
    
    //주머니의 돈이 2000원에서 3500원으로 변경될 예정입니다.
    //주머니의 돈이 2000원에서 3500원으로 변경되었습니다.
    
    dollarInPoket = 3.5 //주머니의 달러를 3.5달러로 변경 중입니다.
}


public func chapter10_12(){
    class AClass{
        //저장 타입 프로퍼티
        static var typeProperty: Int = 0
        
        //저장 인스턴스 프로퍼티
        var instanceProperty: Int = 0{
            didSet{
                AClass.typeProperty = instanceProperty + 100
            }
        }
        
        //연산 타입 프로퍼티
        static var typeComputedProperty: Int{
            get{
                return typeProperty
            }
            
            set{
                typeProperty = newValue
            }
        }
    }
    
    AClass.typeProperty = 123
    
    let classInstance: AClass = AClass()
    classInstance.instanceProperty = 100
    
    print(AClass.typeProperty)
    print(AClass.typeComputedProperty)
}



public func chapter10_13(){
    class Account{
        static let dollarExchangeRate: Double = 100.0   //타입상수
        
        var credit: Int = 0 //저장 인스턴스 프로퍼티
        
        var dollarValue: Double{    //연산 인스턴스 프로퍼티
            get{
                return Double(credit) / Account.dollarExchangeRate
            }
            
            set{
                credit = Int(newValue*Account.dollarExchangeRate)
                print("잔액을 \(newValue)달러로 변경 중입니다.")
                
            }
        }
    }
    
    let classInstance: Account = Account()
    classInstance.credit = 10
    print(classInstance.dollarValue)
    classInstance.dollarValue = 200
    print(classInstance.credit)
}



public func chapter10_14(){
    class LevelClass{
        // 현재 레벨을 저장하는 저장 프로퍼티
        var level: Int = 0{
            didSet{
                // 프로퍼티 값이 변경되면 호출되는 프로퍼티 감시자
                print("Level \(level)")
            }
        }
        
        // 레벨이 올랐을 때 호출할 메서드
        func levelUp(){
            print("Level Up!")
            level += 1
        }
        
        // 레벨이 감소했을 때 호출할 메서드
        func levelDown(){
            print("Level Down")
            level -= 1
            if level < 0 {
                reset()
            }
        }
        
        // 특정 레벨로 이동할 때 호출할 메서드
        func jumpLevel(to: Int){
            print("Jump to \(to)")
            level = to
        }
        
        // 레벨 초기화할 때 호출할 메서드
        func reset(){
            print("Reset!")
            level = 0
        }
        
        
    }
    
    var levelClassInstance: LevelClass = LevelClass()
    levelClassInstance.levelUp()
    levelClassInstance.levelDown()
    levelClassInstance.levelDown()
    levelClassInstance.jumpLevel(to: 3)
    
    
}

public func chapter10_15(){
    struct LevelClass{
        // 현재 레벨을 저장하는 저장 프로퍼티
        var level: Int = 0{
            didSet{
                // 프로퍼티 값이 변경되면 호출되는 프로퍼티 감시자
                print("Level \(level)")
            }
        }
        
        // 레벨이 올랐을 때 호출할 메서드
        mutating func levelUp(){
            print("Level Up!")
            level += 1
        }
        
        // 레벨이 감소했을 때 호출할 메서드
        mutating func levelDown(){
            print("Level Down")
            level -= 1
            if level < 0 {
                reset()
            }
        }
        
        // 특정 레벨로 이동할 때 호출할 메서드
        mutating func jumpLevel(to: Int){
            print("Jump to \(to)")
            level = to
        }
        
        // 레벨 초기화할 때 호출할 메서드
        mutating func reset(){
            print("Reset!")
            level = 0
        }
        
        
    }
    
    var levelClassInstance: LevelClass = LevelClass()
    levelClassInstance.levelUp()
    levelClassInstance.levelDown()
    levelClassInstance.levelDown()
    levelClassInstance.jumpLevel(to: 3)
    
    
}

public func chapter10_17(){
    class LevelClass{
        var level: Int = 0
        
        func reset(){
            // 오류!! self 프로퍼티 참조 변경 불가
            //self = LevelClass()
        }
    }
    
    struct LevelStruce{
        var level: Int = 0
        
        mutating func levelUp(){
            print("Level Up!")
            level += 1
        }
        
        mutating func reset(){
            print("Reset!")
            self = LevelStruce()
        }
    }
    
    var levelStructInstance: LevelStruce = LevelStruce()
    levelStructInstance.levelUp()   //Level Up!
    //level 1
    levelStructInstance.reset()
    print(levelStructInstance.level)
    
    enum OnOffSwitch{
        case on, off
        mutating func nextState(){
            self = self == .on ? .off : .on
        }
    }
    
    var toogle: OnOffSwitch = OnOffSwitch.off
    toogle.nextState()
    print(toogle)   //on
    toogle.nextState()
    print(toogle)   //off
}

public func chapter10_18(){
    class AClass{
        static func staticTypeMethod(){
            print("AClass staticTypeMethod")
        }
        
        class func classTypeMethod(){
            print("AClass classTypeMethod")
        }
    }
    
    class BClass: AClass{
        /* 오류발생!! 재정의 불가 static 타입 메서드는 재정의가 불가능하다.
        override static func staticTypeMethod(){
            
        }
         */
        
        override class func classTypeMethod(){
            print("BClass classTypeMethod")
        }
    }
    
    AClass.staticTypeMethod()
    AClass.classTypeMethod()
    BClass.classTypeMethod()
}

public func chapter10_19(){
    //시스템 음량은 한 기긱에서 유일한 값을 가져야 합니다.
    struct SystemVolume{
        //타입 프로퍼티로 가지게 되면 언제나 유일한 값이 됩니다.
        static var volume: Int = 5
        
        //타입프로퍼티를 제어하기 위해 타입 메서드를 사용합니다.
        static func mute(){
            self.volume = 0 //SystemVolume.volume = 0 과 동일한 표현입니다.
        }
    }
    
    //네비게이션 역할은 여러 인스턴스가 수행할 수 있습니다.
    class Nevigation{
        //네비게이션 인스턴스마다 음량을 따로 설정할 수 있습니다.
        var volume: Int = 5
        
        //길안내 음성 재생
        func guideWay(){
            //네비게이션 외 다른 재생원 음소거
            SystemVolume.mute()
        }
        
        //길안내 음성 종료
        func finishGuideWay(){
            //기존 재생원 음량 복구
            SystemVolume.volume = self.volume
        }
    }
    
    SystemVolume.volume = 10
    
    let myNavi: Nevigation = Nevigation()
    
    myNavi.guideWay()
    print(SystemVolume.volume)
    myNavi.finishGuideWay()
    print(SystemVolume.volume)
}
