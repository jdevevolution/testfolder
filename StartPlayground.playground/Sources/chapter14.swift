import Foundation

class Room{
    var number: Int
    
    init(number: Int){
        self.number = number
    }
}

class Building{
    var name: String
    var room: Room?
    
    init(name: String){
        self.name = name
    }
}

struct Address{
    var province: String
    var city: String
    var street: String
    var building: Building?
    var detailAddress: String?
    
    init(province: String, city: String, street: String){
        self.province = province
        self.city = city
        self.street = street
    }
    
    func fullAddress() -> String?{
        var restAddress: String? = nil
        
        if let buildingInfo: Building = self.building{
            restAddress = buildingInfo.name
        } else if let detail = self.detailAddress {
            restAddress = detail
        }
        
        /**
        if let rest: String = restAddress {
            var fullAddress: String = self.province
            
            fullAddress += " " + self.city
            fullAddress += " " + self.street
            fullAddress += " " + rest
            
            return fullAddress
        } else {
            return nil
        }
 */
        // if 구문을 guard 구문으로 수정
        guard let rest: String = restAddress else {
            return nil
        }
        
        var fullAddress: String = self.province
        
        fullAddress += " " + self.city
        fullAddress += " " + self.street
        fullAddress += " " + rest
        
        return fullAddress
    }
    
    func printAddress(){
        if let address: String = self.fullAddress() {
            print(address)
        }
    }
}

class Persons{
    var name: String
    var address: Address?
    
    init(name: String){
        self.name = name
    }
}
/**
 * 사람의 주소 정보 표현 설계
 */
public func chpater14_1(){
    let yagom: Persons = Persons(name: "yagom")
    
    let yagomRoomViaOptionalChaining: Int? = yagom.address?.building?.room?.number
    
    let yagomRoomViaOptionalUnwraping: Int = yagom.address!.building!.room!.number
}

public func chapter14_4(){
    let yagom: Persons = Persons(name: "yagom")
    
    var roomNumber: Int? = nil
    
    if let yagomAddress: Address = yagom.address{
        if let yagomBuilding: Building = yagomAddress.building{
            if let yagomRoom: Room = yagomBuilding.room{
                roomNumber = yagomRoom.number
            }
        }
    }
    
    if let number: Int = roomNumber{
        print(number)
    } else {
        print("Can not find room number")
    }
}

/**
 * 옵셔널 체이닝의 사용
 */
public func chapter14_5(){
    let yagom: Persons = Persons(name: "yagom")
    
    if let roomNumber: Int = yagom.address?.building?.room?.number{
        print(roomNumber)
    } else {
        print("Can not find room number")
    }
    
    // chapter14-6 추가내용
    yagom.address?.building?.room?.number = 505
    print(yagom.address?.building?.room?.number)
    
    //yagom.address = Address(province: "충청북도", city: "청주시 청원구", street: "충청대로", building: nil, detailAddress: nil)
    yagom.address = Address(province: "충청북도", city: "청주시 청원구", street: "충청대로")
    yagom.address?.building = Building(name: "곰굴")
    yagom.address?.building?.room = Room(number: 0)
    yagom.address?.building?.room?.number = 505
    
    print(yagom.address?.building?.room?.number)
    
    print("14-8 추가===============")
    
    yagom.address?.fullAddress()?.isEmpty
    yagom.address?.printAddress()
}

/**
 * 옵셔널 체이닝을 통한 서브스크립트 호출을 위해서는 ?를 앞에 표기해 주어야한다.
 */
public func chapter14_9(){
    let optionalArray: [Int]? = [1,2,3]
    optionalArray?[1]
    
    var optionalDictionary: [String : [Int]]? = [String: [Int]]()
    optionalDictionary?["numberArray"] = optionalArray
    optionalDictionary?["numberArray"]?[2]
}

/**
 * guard 구문의 옵셔널 바인딩 활용
 */
public func chapter14_11(){
    func greet(_ person: [String: String]){
        guard let name: String = person["name"] else{
            return
        }
        
        print("Hello \(name)!")
        
        guard let location: String = person["location"] else{
            print("I hope the weather is nice near you")
            return
        }
        
        print("I hope the weather is nice in \(location)")
    }
    
    var personInfo: [String: String] = [String: String]()
    personInfo["name"] = "Jenny"
    
    greet(personInfo)
    
    personInfo["location"] = "Korea"
    
    greet(personInfo)
}

/**
 * guard구문에 구체적인 조건을 추가
 */
public func chapter14_13(){
    func enterClub(name: String?, age: Int?){
        guard let name: String = name, let age: Int = age, age > 19, name.isEmpty == false else {
            print("You are too young to enter the club")
            return
        }
        
        print("Welcome \(name)")
    }
    
    enterClub(name: "Jung", age: nil)
    
    enterClub(name: "Yum", age: 22)
    
}

/**
 * guard 구문이 사용될 수 없는 경우
 */
public func chapter14_14(){
    let first: Int = 3
    let second: Int = 5
    
    guard first > second else {
        print("error")
        return
    }
}
