import Foundation

let names: [String] = ["wizplan", "eric", "yagom", "jenny"]

public func chapter13_3(){
    func backwards(first: String, second: String) -> Bool{
        print("\(first) \(second) 비교중")
        return first < second
    }
    
    let reversed: [String] = names.sorted(by: backwards)
    print(reversed)
}

/**
 * sorted(by:) 메서드에 클러저 전달
 */
public func chapter13_4(){
    let reversed: [String] = names.sorted(by: {(first: String, second: String) -> Bool in
        return first > second
    })
    print(reversed)
}
/**
 * 후행 클러저 표현
 */
public func chapter13_5(){
    let reversed: [String] = names.sorted(){(first: String, second: String) -> Bool in
        return first > second
    }
    //sorded(by:) 메서드에의 소괄호까지 생각 가능
    let reversed2: [String] = names.sorted{(first: String, second: String) -> Bool in
        return first > second
    }
    print(reversed)
    print(reversed2)
}

/**
 * 클로저의 타입 유추
 */
public func chapter13_6(){
    //클러저의 매개변수 타입과 반환 타입을 생략하여 표현할 수 있습니다.
    let reversed: [String] = names.sorted{ (first, second) in
        return first > second
    }
    print(reversed)
}

/**
 * 단축 인자 이름 사용
 */
public func chapter13_7(){
    let reversed: [String] = names.sorted{
        return $0 > $1
    }
    print(reversed)
}

/**
 * 암시적 반환 표현의 사용
 */
public func chapter13_8(){
    let reversed: [String] = names.sorted { $0 > $1 }
    print(reversed)
}

/**
 * 클로저로서의 연산자 함수 사용
 */
public func chapter13_10(){
    let reversed: [String] = names.sorted(by: >)
    print(reversed)
}

public func chapter13_11(){
    func makeIncrementer(forIncrement amount: Int) -> (()->Int){
        var runningTotal = 0
        func incrementer() -> Int{
            runningTotal += amount
            return runningTotal
        }
        return incrementer
    }
    
    let incrementByTwo: (() -> Int) = makeIncrementer(forIncrement: 2)
    
    let first: Int = incrementByTwo()
    let second: Int = incrementByTwo()
    let third: Int = incrementByTwo()
    
    print("\(first), \(second), \(third)")
}

/**
 * 탈출 클로저를 매개변수로 갖는 함수
 */
public func chapter13_16(){
    var completionHandlers: [() -> Void] = []
    
    func someFunctionWithEscaping(completionHandler: @escaping () -> Void){
        completionHandlers.append(completionHandler)
    }
}

/**
 * 함수를 탈출하는 클로저의 예
 */
public func chapter13_17(){
    typealias VoidVoidClosure = () -> Void
    
    let firstClosure: VoidVoidClosure = {
        print("Closure A")
    }
    
    let secondClosure: VoidVoidClosure = {
        print("Closure B")
    }
    
    // first와 second 매개변수 클로저는 함수의 반환 값으로 사용될 수 있으므로 탈출 클로저입니다.
    func returnOneClosure(first: @escaping VoidVoidClosure, second: @escaping VoidVoidClosure, shouldReturnFirstClosure: Bool) -> VoidVoidClosure {
        // 전달인자로 전달받은 클로저가 함수 외부로 다시 반환되기 때문에 함수를 탈출하는 클로저입니다.
        return shouldReturnFirstClosure ? first : second
    }
    
    // 함수에서 반환된 클로저가 함수 외부의 상수에 저장되었습니다.
    let returnedClosure: VoidVoidClosure = returnOneClosure(first: firstClosure, second: secondClosure, shouldReturnFirstClosure: true)
    
    returnedClosure()   // Closure A
    
    var closures: [VoidVoidClosure] = []
    
    // closure 매개변수 클로저는 함수 외부의 변수에 저장될 수 있으므로 탈출 클로저입니다.
    func appendClosure(closure: @escaping VoidVoidClosure){
        // 전달인자로 전달받은 클로저가 함수 외부의 변수 내부에 저장되므로 함수를 탈출합니다.
        closures.append(closure)
    }
}


/**
 * 클래스 인스턴스 메서드에 사용되는 탈출, 비탈출 클로저
 */
public func chapter13_18(){
    typealias VoidVoidClosure = () -> Void
    
    class SomeClass{
        var x = 10
        func functionWithNoescapeClosure(closure: VoidVoidClosure){
            closure()
        }
        
        func functionWithEscapingClosure(completionHandler: @escaping VoidVoidClosure) -> VoidVoidClosure{
            return completionHandler
        }
        
        func runNoescapeClosure() {
            // 비탈출 클로저에서 self 사용은 선택 사항입니다.
            functionWithNoescapeClosure { x = 200 }
        }
        
        func runEscapingClosure() -> VoidVoidClosure {
            // 탈출 클로저에서는 명시적으로 self를 사용해야 합니다.
            return functionWithEscapingClosure{self.x = 100}
        }
    }
    
    let instance: SomeClass = SomeClass()
    instance.runNoescapeClosure()
    print(instance.x)   //200
    
    let returnedClosure: VoidVoidClosure = instance.runEscapingClosure()
    returnedClosure()
    print(instance.x)   //100
}

/**
 * 클로저를 이용한 연산지연
 */
public func chapter13_19(){
    //대기중인 손님들입니다.
    var customersInLine: [String] = ["YoangWha", "sangYong", "SungHun", "HaMi"]
    print(customersInLine.count)
    
    // 클로저를 만들어두면 클로저 내부의 코드를 미리 실행(연산)하지 않고 가지고만 있습니다.
    let customerProvider: () -> String = {
        return customersInLine.removeFirst()
    }
    print(customersInLine.count)
    
    // 실제로 실행합니다.
    print("Now serving \(customerProvider())!")
    print(customersInLine.count)
    
}

/**
 * 함수의 전달인자로 전달되는 클로저
 */
public func chapter13_20(){
    //대기중인 손님들입니다.
    var customersInLine: [String] = ["YoangWha", "sangYong", "SungHun", "HaMi"]
    print(customersInLine.count)
    
    func serveCustomer(_ customerProvider: () -> String){
        print("Now serving \(customerProvider())")
    }
    serveCustomer( {customersInLine.removeFirst()} )
}

/**
 * 자동 클로저의 사용
 */
public func chapter13_21(){
    //대기중인 손님들입니다.
    var customersInLine: [String] = ["YoangWha", "sangYong", "SungHun", "HaMi"]
    
    func serveCustomer(_ customerProvider: @autoclosure () -> String){
        print("Now serving \(customerProvider())")
    }
    serveCustomer(customersInLine.removeFirst())
}

/**
 * 자동 클로저의 탈출
 */
public func chapter13_22(){
    var customersInLine: [String] = ["minJae", "innoceive", "sopress"]
    
    func returnProvider(_ customerProvider: @autoclosure @escaping () -> String) -> (() -> String){
        return customerProvider
    }
    
    let customerProvider: () -> String = returnProvider(customersInLine.removeFirst())
    print("Now serving \(customerProvider())!")
}
